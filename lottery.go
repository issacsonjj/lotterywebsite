// LotteryInfoCollector project main.go
package main

import (
	"errors"
	"fmt"
	iconv "github.com/djimenez/iconv-go"
	"github.com/go-martini/martini"
	"github.com/martini-contrib/render"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type MatchInfo struct {
	HomeTeam  string
	GuestTeam string
	Ts        time.Time
	TimeStr   string
	BtrWin    float32
	BtrPlain  float32
	BtrFail   float32
	BtrWinM   float32
	BtrPlainM float32
	BtrFailM  float32
	AllowBet  int64

	InfoHref string

	DetailInfo *MatchDetailedInfo
}

func performHttpGet(url string) (res string, err error) {
	err = nil
	res = ""

	resp, err := http.Get(url)
	if err != nil {
		return
	}

	defer resp.Body.Close()
	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("read fail: ", err.Error())
		return
	}

	outBytes := make([]byte, len(respBytes)*3/2)
	_, nWrite, err := iconv.Convert(respBytes, outBytes, "GB2312", "utf-8")
	if err != nil {
		fmt.Println("convert fail: ", err.Error())
		return
	}

	res = string(outBytes[0:nWrite])
	re := regexp.MustCompile("\\<script[\\S\\s]*?\\</script\\>")
	res = re.ReplaceAllString(res, "")
	res = strings.Replace(res, "gb2312", "utf-8", 1)
	//去掉style
	re = regexp.MustCompile("\\<style[\\S\\s]+?\\</style\\>")
	res = re.ReplaceAllString(res, "")
	//去掉注释
	re = regexp.MustCompile("\\<!--[\\S\\s]+?--\\>")
	res = re.ReplaceAllString(res, "")
	re = regexp.MustCompile("\\<[\\S\\s]+?\\>")
	res = re.ReplaceAllStringFunc(res, strings.ToLower)

	return
}

//比赛时间
func getMatchTime(resp string) (times []time.Time, err error) {
	err = nil
	times = nil

	re := regexp.MustCompile("\\<td[\\s]*?align=\"center\"[\\s]*?class=\"switchtime[\\s]*?timetd[\\s]*?td2[\\s]*?\"[\\s]title=\"比赛时间：(?P<time>\\d{4}-\\d{1,2}-\\d{1,2}[\\s]+?\\d{1,2}:\\d{1,2}:\\d{1,2})[\\s]*?\"[\\s]*?\\>[\\s]*?\\<span[\\s]*?class=\".+?\"[\\s]+?style=\".+?\"[\\s]*?\\>.+?\\</span\\>[\\s]*?\\<span[\\s]+?class=\"buytime\"[\\s]*?\\>.+?\\</span\\>[\\s]*?\\</td\\>")
	timeArr := re.FindAllString(resp, -1)
	if len(timeArr) <= 0 {
		err = errors.New("no result matched")
		return
	} else {
		times = make([]time.Time, len(timeArr))
		timeCnt := 0
		for _, str := range timeArr {
			matchTimes := re.FindStringSubmatch(str)
			if len(matchTimes) <= 1 {
				err = errors.New("no result matched")
				return
			}

			t, err := time.Parse("2006-01-02 15:04:05 -0700", matchTimes[1]+" +0800")
			if err != nil {
				return times, err
			}

			//if t.After(today) {
			times[timeCnt] = t
			timeCnt++
			//}
		}

		return times[0:timeCnt], err
	}
}

//对阵
func getTeams(resp string) (teams [][2]string, err error) {
	teams = nil
	err = nil

	re := regexp.MustCompile("\\<a[\\s]+?class=\"float_l font_14[\\s]+?tar[\\s]+?duinameh\"[\\s]+?href=\".+?\"[\\s]+?target=\".+?\"[\\s]+?title=\".*?\"\\>(?P<subject>.+?)\\</a\\>[\\s\\S]*?\\<a[\\s\\S]+?\\>(?P<object>.+?)\\</a\\>")
	teamArr := re.FindAllString(resp, -1)
	if len(teamArr) <= 0 {
		err = errors.New("no matched info")
		return
	}

	teams = make([][2]string, len(teamArr))
	teamCnt := 0
	for _, teamStr := range teamArr {
		matchTeams := re.FindStringSubmatch(teamStr)
		//fmt.Println(len(matchTeams))
		if len(matchTeams) < 3 {
			err = errors.New("invalid result")
			return teams, err
		}

		teams[teamCnt][0] = matchTeams[1]
		teams[teamCnt][1] = matchTeams[2]
		teamCnt++
	}

	return teams[0:teamCnt], err
}

func getBetrate(resp string) (bets [][2][3]string, rq []string, err error) {
	err = nil
	bets = nil
	rq = nil

	re := regexp.MustCompile("\\<td class=\"td[\\s\\S]*?zqmixztbox[\\s\\S]*?\\</td\\>")
	oddsArr := re.FindAllString(resp, -1)
	oddsSearchStr := "\\<a class=\"betobj[\\s\\S]+?\\>[\\s]*(?P<betrate>.+?)[\\s]*\\</a\\>"
	oddsRqStr := "\\<a class=\"rqbetobj[\\s\\S]+?\\>[\\s]*(?P<betrate>.+?)[\\s]*\\</a\\>"
	betValStr := "\\<div[\\s]+?class=\"[\\s\\S]+?handicapobj[\\s\\S]+?\"\\>(?P<bet>.+?)\\</div\\>"
	if len(oddsArr) <= 0 {
		err = errors.New("no matched info")
		return bets, rq, err
	}

	bets = make([][2][3]string, len(oddsArr))
	rq = make([]string, len(oddsArr))
	i := 0
	for _, odds := range oddsArr {
		//fmt.Println(odds)
		betRe := regexp.MustCompile(oddsSearchStr)
		rqBetRe := regexp.MustCompile(oddsRqStr)
		betValRe := regexp.MustCompile(betValStr)

		betArr := betRe.FindAllString(odds, -1)       //3 items
		betRqArr := rqBetRe.FindAllString(odds, -1)   //3 items
		betValArr := betValRe.FindAllString(odds, -1) //1 items
		if len(betArr) != 3 || len(betRqArr) != 3 || len(betValArr) != 1 {
			//errStr := fmt.Sprintf("betarr = %d, betrqarr = %d, betvalarr = %d, i = %d", len(betArr), len(betRqArr), len(betValArr), i)
			//err = errors.New(errStr)
			//return bets, rq, err
			i++
			continue
		}

		for j := 0; j < 3; j++ {
			betArrSub := betRe.FindStringSubmatch(betArr[j])
			betRqArrSub := rqBetRe.FindStringSubmatch(betRqArr[j])
			bets[i][0][j] = betArrSub[1]
			bets[i][1][j] = betRqArrSub[1]
		}

		betValArrSub := betValRe.FindStringSubmatch(betValArr[0])

		rq[i] = betValArrSub[1]
		i++
	}

	return bets, rq, err
}

func getHrefs(resp string) (hrefs []string, err error) {
	hrefs = nil
	err = nil

	hrefStr := "http://www\\.okooo\\.com/soccer/match/\\d+/trends/"
	hrefRe := regexp.MustCompile(hrefStr)

	hrefArr := hrefRe.FindAllString(resp, -1)
	arrLen := len(hrefArr)
	if arrLen <= 0 {
		return hrefs, errors.New("no result matched")
	}
	hrefs = make([]string, arrLen)
	for i, s := range hrefArr {
		hrefs[i] = strings.Replace(s, "trends", "exchanges", -1)
		fmt.Println(hrefs[i])
	}

	return hrefs, err
}

func getLotteryInfo() (info [](*MatchInfo), err error) {
	info = nil
	err = nil
	respStr, err := performHttpGet("http://www.okooo.com/jingcai/shengpingfu/")
	if err != nil {
		fmt.Println("error: ", err)
		return
	}

	times, err := getMatchTime(respStr)
	if err != nil {
		fmt.Println("error: ", err)
		return
	}
	timeCnt := len(times)

	teams, err := getTeams(respStr)
	if err != nil {
		fmt.Println("error: ", err)
		return
	}

	teamCnt := len(teams)

	//赔率
	bets, rq, err := getBetrate(respStr)
	if err != nil {
		fmt.Println("error: ", err)
		return
	}
	betCnt := len(bets)

	hrefs, err := getHrefs(respStr)
	hrefCnt := len(hrefs)

	if timeCnt != teamCnt || betCnt != timeCnt || timeCnt != hrefCnt {
		fmt.Printf("error: time count = %d, team count = %d, bet count = %d, href count = \n", timeCnt, teamCnt, betCnt, hrefCnt)
		return
	}

	/*
		type MatchInfo struct {
		HomeTeam  string
		GuestTeam string
		Ts        time.Time
		BtrWin    float32
		BtrPlain  float32
		BtrFail   float32
		BtrWinM   float32
		BtrPlainM float32
		BtrFailM  float32
		AllowBet  int
	*/

	info = make([](*MatchInfo), timeCnt)
	enableArr := make([]bool, timeCnt)
	validInfoCnt := 0
	nowTime := time.Now()
	for i := 0; i < betCnt; i++ {
		enableArr[i] = true
		matchInfo := new(MatchInfo)
		info[i] = matchInfo
		matchInfo.HomeTeam = teams[i][0]
		matchInfo.GuestTeam = teams[i][1]
		matchInfo.Ts = times[i]
		if times[i].Before(nowTime) {
			enableArr[i] = false
		}
		matchInfo.TimeStr = times[i].Format("2006-01-02 15:04:05")
		var tmpFloat64 float64
		tmpFloat64, _ = strconv.ParseFloat(bets[i][0][0], 32)
		if bets[i][0][0] == "" {
			tmpFloat64 = -1000.0
			enableArr[i] = false
		}
		matchInfo.BtrWin = float32(tmpFloat64)
		tmpFloat64, _ = strconv.ParseFloat(bets[i][0][1], 32)
		matchInfo.BtrPlain = float32(tmpFloat64)
		tmpFloat64, _ = strconv.ParseFloat(bets[i][0][2], 32)
		matchInfo.BtrFail = float32(tmpFloat64)
		tmpFloat64, _ = strconv.ParseFloat(bets[i][1][0], 32)
		matchInfo.BtrWinM = float32(tmpFloat64)
		tmpFloat64, _ = strconv.ParseFloat(bets[i][1][1], 32)
		matchInfo.BtrPlainM = float32(tmpFloat64)
		tmpFloat64, _ = strconv.ParseFloat(bets[i][1][2], 32)
		matchInfo.BtrFailM = float32(tmpFloat64)
		matchInfo.AllowBet, _ = strconv.ParseInt(rq[i], 0, 32)

		matchInfo.InfoHref = hrefs[i]

		if enableArr[i] {
			detailResp, err := performHttpGet(matchInfo.InfoHref)
			if err != nil {
				fmt.Println("error: performHttpGet detailResp fail")
				return info, err
			}

			detailInfo, err := parseExchangeInfo(detailResp)
			if err != nil {
				enableArr[i] = false
				fmt.Println("err: ", err)
			} else {
				//itemStr := dumpItemInfo(detailInfo.InfoWin)
				//fmt.Println(itemStr)
				//itemStr = dumpItemInfo(detailInfo.InfoPlain)
				//fmt.Println(itemStr)
				//itemStr = dumpItemInfo(detailInfo.InfoFail)
				//fmt.Println(itemStr)

				//fmt.Println(matchInfo.DetailInfo.InfoWin)
			}

			matchInfo.DetailInfo = detailInfo
		}

		if enableArr[i] {
			validInfoCnt++
		}
	}

	validInfo := make([]*MatchInfo, validInfoCnt)
	validIndex := 0
	for i, enabled := range enableArr {
		if enabled {
			validInfo[validIndex] = info[i]
			validIndex++
		}
	}

	info = validInfo

	return
}

func dumpMatchInfo(match *MatchInfo) string {
	return fmt.Sprintf("time: %s, %s vs %s, bet: %f %f %f, rg: %d, rq bet: %f %f %f", match.Ts.Format("2006-01-02 15:04:05"), match.HomeTeam, match.GuestTeam, match.BtrWin, match.BtrPlain, match.BtrFail, match.AllowBet, match.BtrWinM, match.BtrPlainM, match.BtrFailM)
}

//赔率
func main() {
	m := martini.Classic()
	m.Use(render.Renderer())
	m.Get("/index", func(r render.Render) {
		info, _ := getLotteryInfo()
		mp := make(map[string]interface{})
		mp["data"] = info
		r.HTML(200, "index", mp)
	})

	m.Run()
}
