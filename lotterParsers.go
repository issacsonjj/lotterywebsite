// lotterParsers
package main

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
)

type MatchDetailInfoItem struct {
	//必发
	PriceBuyer    float32
	BuyAmount     int64
	PriceSeller   float32
	SellAmount    int64
	BfPrice       float32
	BfTransAmount int64
	BfBankProfit  int64

	//竞彩
	JcOdds       float32
	JcTrans      int64
	JcBankProfit int64
	JcPlayIndex  int64
	JcBillIndex  int64

	//99家平均
	AvgOdds float32
	Possi   float32

	//交易量比例
	BfTransRatio float32
	JcTransRatio float32
	BdTransRatio float32

	//交易冷热指数
	BfTransIndex int64
	JcTransIndex int64

	//市场指数
	BfMarketIndex int64
	JcMarketIndex int64

	//出售倾向
	BfSellTrend int64
	BfBuyTrend  int64
	JcBuyTrend  int64
}

func dumpItemInfo(info MatchDetailInfoItem) string {
	/*
		PriceBuyer    float32
		BuyAmount     int64
		PriceSeller   float32
		SellAmount    int64
		BfPrice       float32
		BfTransAmount int64
		BfBankProfit  int64

		//竞彩
		JcOdds       float32
		JcTrans      int64
		JcBankProfit int64
		JcPlayIndex  int64
		JcBillIndex  int64
	*/

	result := fmt.Sprintf("PriceBuyer: %f, BuyAmount: %d, PriceSeller: %f, SellAmount: %d, "+
		"BfPrice: %f, BfTransAmount: %d, BfBankProfit: %d, "+
		"JcOdds: %f, JcTrans: %d, JcBandkProfit: %d, JcPlayIndex: %d, JcBillIndex: %d",
		info.PriceBuyer, info.BuyAmount, info.PriceSeller, info.SellAmount,
		info.BfPrice, info.BfTransAmount, info.BfBankProfit,
		info.JcOdds, info.JcTrans, info.JcBankProfit, info.JcPlayIndex, info.JcBillIndex)

	return result
}

type BallInfo struct {
	BfBuyPrice     float32
	BfBuyAmount    int64
	BfSellPrice    float32
	BfSellAmount   int64
	BfTransPrice   float32
	BfTransAmount  int64
	BfBankerProfit int64

	//99家平均
	AvgOdds float32
	Possi   float32

	TransRatio  float32
	TransTrend  int64
	BfIndex     int64
	ProfitIndex int64
}

type MatchDetailedInfo struct {
	InfoWin   MatchDetailInfoItem
	InfoPlain MatchDetailInfoItem
	InfoFail  MatchDetailInfoItem

	BigBallInfo    BallInfo
	LittleBallInfo BallInfo
}

func getFloatValue(f string) float32 {
	result, _ := strconv.ParseFloat(f, 32)

	return float32(result)
}

func getIntValue(i string) int64 {
	result, _ := strconv.ParseInt(i, 0, 64)

	return result
}

//解析一个MatchDetailInfoItem
func parseItem(str string, item *MatchDetailInfoItem) error {
	bdrLftReStr := "\\<td[\\s]+?class=\"borderleft\"\\>(?P<nu>.+?)\\</td\\>"
	bdrRhtReStr := "\\<td[\\s]+?class=\"borderright\">(?P<nu>.+?)\\</td\\>"
	trReStr := "\\<td\\>(?P<nu>.+?)\\</td\\>"

	bdrLftRe := regexp.MustCompile(bdrLftReStr)
	bdrRhtRe := regexp.MustCompile(bdrRhtReStr)
	trRe := regexp.MustCompile(trReStr)

	lftArr := bdrLftRe.FindAllString(str, -1)
	rhtArr := bdrRhtRe.FindAllString(str, -1)
	trArr := trRe.FindAllString(str, -1)
	lftArrLen := len(lftArr)
	rhtArrLen := len(rhtArr)
	trArrLen := len(trArr)
	if lftArrLen != 4 || rhtArrLen != 3 || trArrLen != 5 {
		return errors.New(fmt.Sprintf("parseItem invalid format, lftArrLen: %d, rhtArrLen: %d, trArrLen: %d",
			lftArrLen, rhtArrLen, trArrLen))
	}

	//border left, 浮点类型
	//PriceBuyer
	//PriceSeller
	//BfPrice
	//JcOdds
	lftArrSub := bdrLftRe.FindStringSubmatch(lftArr[0])
	if len(lftArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , lftArrSub len: ", len(lftArrSub)))
	}
	item.PriceBuyer = getFloatValue(lftArrSub[1])

	lftArrSub = bdrLftRe.FindStringSubmatch(lftArr[1])
	if len(lftArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , lftArrSub len: ", len(lftArrSub)))
	}
	item.PriceSeller = getFloatValue(lftArrSub[1])

	lftArrSub = bdrLftRe.FindStringSubmatch(lftArr[2])
	if len(lftArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , lftArrSub len: ", len(lftArrSub)))
	}
	item.BfPrice = getFloatValue(lftArrSub[1])

	lftArrSub = bdrLftRe.FindStringSubmatch(lftArr[3])
	if len(lftArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , lftArrSub len: ", len(lftArrSub)))
	}
	item.JcOdds = getFloatValue(lftArrSub[1])

	//border right，整型
	//BuyAmount
	//SellAmount
	//BfBankProfit
	rhtArrSub := bdrRhtRe.FindStringSubmatch(rhtArr[0])
	if len(rhtArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , rhtArrSub len: ", len(rhtArrSub)))
	}
	item.BuyAmount = getIntValue(rhtArrSub[1])

	rhtArrSub = bdrRhtRe.FindStringSubmatch(rhtArr[1])
	if len(rhtArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , rhtArrSub len: ", len(rhtArrSub)))
	}
	item.SellAmount = getIntValue(rhtArrSub[1])

	rhtArrSub = bdrRhtRe.FindStringSubmatch(rhtArr[2])
	if len(rhtArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , rhtArrSub len: ", len(rhtArrSub)))
	}
	item.BfBankProfit = getIntValue(rhtArrSub[1])

	//tr
	//BfTransAmount
	//JcTrans
	//JcBankProfit
	//JcPlayIndex
	//JcBillIndex
	trArrSub := trRe.FindStringSubmatch(trArr[0])
	if len(trArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , trArrSub len: ", len(trArrSub)))
	}
	item.BfTransAmount = getIntValue(trArrSub[1])

	trArrSub = trRe.FindStringSubmatch(trArr[1])
	if len(trArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , trArrSub len: ", len(trArrSub)))
	}
	item.JcTrans = getIntValue(trArrSub[1])

	trArrSub = trRe.FindStringSubmatch(trArr[2])
	if len(trArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , trArrSub len: ", len(trArrSub)))
	}
	item.JcBankProfit = getIntValue(trArrSub[1])

	trArrSub = trRe.FindStringSubmatch(trArr[3])
	if len(trArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , trArrSub len: ", len(trArrSub)))
	}
	item.JcPlayIndex = getIntValue(trArrSub[1])

	trArrSub = trRe.FindStringSubmatch(trArr[4])
	if len(trArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , trArrSub len: ", len(trArrSub)))
	}
	item.JcBillIndex = getIntValue(trArrSub[1])

	return nil
}

//解析MatchDetailInfoItem下半部分
func parseItem1(str string, item *MatchDetailInfoItem) error {
	bdrLftReStr := "\\<td[\\s]+?class=\"borderLeft\"\\>(?P<nu>.+?)\\</td\\>"
	bdrRhtReStr := "\\<td[\\s]+?class=\"borderRight\">(?P<nu>.+?)\\</td\\>"
	trReStr := "\\<td\\>(?P<nu>.+?)\\</td\\>"

	bdrLftRe := regexp.MustCompile(bdrLftReStr)
	bdrRhtRe := regexp.MustCompile(bdrRhtReStr)
	trRe := regexp.MustCompile(trReStr)

	lftArr := bdrLftRe.FindAllString(str, -1)
	rhtArr := bdrRhtRe.FindAllString(str, -1)
	trArr := trRe.FindAllString(str, -1)
	lftArrLen := len(lftArr)
	rhtArrLen := len(rhtArr)
	trArrLen := len(trArr)
	if lftArrLen != 4 || rhtArrLen != 3 || trArrLen != 5 {
		return errors.New(fmt.Sprintf("parseItem invalid format, lftArrLen: %d, rhtArrLen: %d, trArrLen: %d",
			lftArrLen, rhtArrLen, trArrLen))
	}

	//border left, 浮点类型
	//AvgOdds
	//BfTransRatio
	//BfPrice
	//JcOdds
	lftArrSub := bdrLftRe.FindStringSubmatch(lftArr[0])
	if len(lftArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , lftArrSub len: ", len(lftArrSub)))
	}
	item.PriceBuyer = getFloatValue(lftArrSub[1])

	lftArrSub = bdrLftRe.FindStringSubmatch(lftArr[1])
	if len(lftArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , lftArrSub len: ", len(lftArrSub)))
	}
	item.PriceSeller = getFloatValue(lftArrSub[1])

	lftArrSub = bdrLftRe.FindStringSubmatch(lftArr[2])
	if len(lftArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , lftArrSub len: ", len(lftArrSub)))
	}
	item.BfPrice = getFloatValue(lftArrSub[1])

	lftArrSub = bdrLftRe.FindStringSubmatch(lftArr[3])
	if len(lftArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , lftArrSub len: ", len(lftArrSub)))
	}
	item.JcOdds = getFloatValue(lftArrSub[1])

	//border right，整型
	//BuyAmount
	//SellAmount
	//BfBankProfit
	rhtArrSub := bdrRhtRe.FindStringSubmatch(rhtArr[0])
	if len(rhtArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , rhtArrSub len: ", len(rhtArrSub)))
	}
	item.BuyAmount = getIntValue(rhtArrSub[1])

	rhtArrSub = bdrRhtRe.FindStringSubmatch(rhtArr[1])
	if len(rhtArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , rhtArrSub len: ", len(rhtArrSub)))
	}
	item.SellAmount = getIntValue(rhtArrSub[1])

	rhtArrSub = bdrRhtRe.FindStringSubmatch(rhtArr[2])
	if len(rhtArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , rhtArrSub len: ", len(rhtArrSub)))
	}
	item.BfBankProfit = getIntValue(rhtArrSub[1])

	//tr
	//BfTransAmount
	//JcTrans
	//JcBankProfit
	//JcPlayIndex
	//JcBillIndex
	trArrSub := trRe.FindStringSubmatch(trArr[0])
	if len(trArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , trArrSub len: ", len(trArrSub)))
	}
	item.BfTransAmount = getIntValue(trArrSub[1])

	trArrSub = trRe.FindStringSubmatch(trArr[1])
	if len(trArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , trArrSub len: ", len(trArrSub)))
	}
	item.JcTrans = getIntValue(trArrSub[1])

	trArrSub = trRe.FindStringSubmatch(trArr[2])
	if len(trArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , trArrSub len: ", len(trArrSub)))
	}
	item.JcBankProfit = getIntValue(trArrSub[1])

	trArrSub = trRe.FindStringSubmatch(trArr[3])
	if len(trArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , trArrSub len: ", len(trArrSub)))
	}
	item.JcPlayIndex = getIntValue(trArrSub[1])

	trArrSub = trRe.FindStringSubmatch(trArr[4])
	if len(trArrSub) != 2 {
		return errors.New(fmt.Sprintf("invalid format , trArrSub len: ", len(trArrSub)))
	}
	item.JcBillIndex = getIntValue(trArrSub[1])

	return nil
}

//主要部分
func parseMainData(resp string, info *MatchDetailedInfo) error {
	mainReStr := "\\<div[\\s]+?class=\"mb10[\\s]+?bftable\"\\>[\\s\\S]+?\\</div\\>"
	mainRe := regexp.MustCompile(mainReStr)

	dataArr := mainRe.FindAllString(resp, -1)
	if len(dataArr) != 2 {
		fmt.Println("invalid main string array, length = ", len(dataArr))
		return errors.New("invalid format")
	}

	subReStr1 := "\\<tr\\>[\\s\\S]+?\\</tr\\>"
	subRe := regexp.MustCompile(subReStr1)

	subArr1 := subRe.FindAllString(dataArr[0], -1)
	if len(subArr1) != 3 {
		return errors.New(fmt.Sprintf("invalid format, subitem count = ", len(subArr1)))
	}

	err := parseItem(subArr1[0], &info.InfoWin)
	if err != nil {
		return err
	}

	err = parseItem(subArr1[1], &info.InfoPlain)
	if err != nil {
		return err
	}

	err = parseItem(subArr1[2], &info.InfoFail)
	if err != nil {
		return err
	}

	return err
}

func parseExchangeInfo(resp string) (info *MatchDetailedInfo, err error) {
	info = new(MatchDetailedInfo)
	err = parseMainData(resp, info)
	return info, err
}
